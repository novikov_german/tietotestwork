<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>  
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html>
	<head>

	<link rel="stylesheet" type="text/css" href="<c:url value="/resources/bootstrap-3.3.7-dist/css/bootstrap.css" />" media="all" />
	<script src="<c:url value="/resources/jquery/jquery-3.2.0.js" />"></script>
<script src="<c:url value="/resources/bootstrap-3.3.7-dist/js/bootstrap.js" />"></script>
		<meta charset="utf-8">
		<title>German Novikov</title>
		<script>
			function setSortingParameter(sortByColumn) {
				var sortOrder;
				if ("<%= request.getParameter("sortOrder") %>" == "ASC" 
						&& "<%= request.getParameter("sortByColumn") %>" == sortByColumn ){
				 	sortOrder = "DESC";
				 	window.location.href ="/TietoTestWork/sort?sortByColumn=" + sortByColumn + "&sortOrder=" + sortOrder;
				} else if ("<%= request.getParameter("sortOrder") %>" == "DESC" 
					&& "<%= request.getParameter("sortByColumn") %>" == sortByColumn ){
				 	window.location.href ="/TietoTestWork";
				 	
				} else {
				 	sortOrder = "ASC";
				 	window.location.href ="/TietoTestWork/sort?sortByColumn=" + sortByColumn + "&sortOrder=" + sortOrder;
				}
				
			}
			
			function loadPage(){
				var order;
				if ("<%= request.getParameter("sortOrder") %>" == "ASC"){
				 	order = "&#8595;";
				} else if ("<%= request.getParameter("sortOrder") %>" == "DESC"){
				 	order = "&#8593";
				}
				document.getElementById('<%= request.getParameter("sortByColumn") %>').innerHTML =
				document.getElementById('<%= request.getParameter("sortByColumn") %>').innerHTML + order;
			}
			
			function search(){
				window.location.href ="/TietoTestWork/search?search=" + document.getElementById('search').value;
			}
		</script>
	</head> 
	<body onload="loadPage()">
	<div class="page-header"> <h1 class="text-center"> German Novikov</h1> <div>
	<div class="row">
		<div class="col-md-3"></div>
		<div class="col-md-6">
			<div class="text-right">
				<input type="text" id="search" placeholder="Search"><input class="btn btn-default" type="button" value="Search" onclick="search();">
			</div>
			<table class="table table-striped">
				<tr>
					<th id="name" ><a onclick="setSortingParameter('name');">Name</a></th>
					<th id="address" ><a onclick="setSortingParameter('address');">Address</a> </th>
					<th id="avg_rating"><a onclick="setSortingParameter('avg_rating');">Rating</a></th>
					<th id="number_of_votes"><a onclick="setSortingParameter('number_of_votes');">Number of votes</a></th>
				</tr>
				
				<c:forEach items="${eatingPlaces}" var="eatingPlace">
					<tr>
						<td><a href="eatingPlace?eatingPlaceId=${eatingPlace.id}"> ${eatingPlace.name}</a></td>
						<td>${eatingPlace.address}</td>
						<td>${eatingPlace.avgRating}</td>
						<td>${eatingPlace.numberOfVotes}</td>
					</tr>
				</c:forEach>
			</table>
			<div>
			<c:url var="navFormUrl" value= "/navigate" />
			<form:form method="post" action="${navFormUrl}">
                <input type="submit" name="action" value="previous" class="btn btn-info" />
                <input type="submit" name="action" value="next" class="btn btn-info" />

            </form:form>
			</div>
		</div>
		<div class="col-md-3"></div>
	</div>
	</body>
</html>
