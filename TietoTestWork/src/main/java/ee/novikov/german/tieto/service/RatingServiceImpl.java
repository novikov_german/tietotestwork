package ee.novikov.german.tieto.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ee.novikov.german.tieto.dao.RatingDao;
import ee.novikov.german.tieto.model.Rating;

@Service
public class RatingServiceImpl implements RatingService {
	
	@Autowired
	private RatingDao ratingDao;
	
	@Transactional
	public void addRating(Rating rating) {
		ratingDao.addRating(rating);
	}

	@Transactional
	public List getAllRatingWhereEatingPlaceId(int eatingPlaceId) {
		return ratingDao.getAllRatingWhereEatingPlaceId(eatingPlaceId);
	}

//	@Override
//	@Transactional
//	public Rating getRatingById(int id) {
//		return ratingDao.getRatingById(id);
//	}
	
}
