package ee.novikov.german.tieto.dao;

import java.util.List;

import ee.novikov.german.tieto.model.EatingPlace;

public interface EatingPlaceDao {
	public List<EatingPlace> getAllEatingPlaces();
	public EatingPlace getEatingPlaceById(int eatingPlaceId);
	public List<EatingPlace> getAllEatingPlacesOrder(String sortByColumn, String sortOrder);
	public List<EatingPlace> searchEatingPlace(String search);
}
