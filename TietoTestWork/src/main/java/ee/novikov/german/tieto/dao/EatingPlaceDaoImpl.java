package ee.novikov.german.tieto.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import ee.novikov.german.tieto.model.EatingPlace;
import ee.novikov.german.tieto.model.Rating;

@Repository
public class EatingPlaceDaoImpl implements EatingPlaceDao {
	
	@Autowired
    private SessionFactory sessionFactory;
	
	public List<EatingPlace> getAllEatingPlaces() {
		return sessionFactory.getCurrentSession().createQuery("from EatingPlace").list();
	}

	public EatingPlace getEatingPlaceById(int eatingPlaceId) {
		return (EatingPlace) sessionFactory.getCurrentSession().get(EatingPlace.class, eatingPlaceId);
	}

	public List<EatingPlace> getAllEatingPlacesOrder(String sortByColumn, String sortOrder) {
		return sessionFactory.getCurrentSession()
				.createQuery("from EatingPlace order by " + sortByColumn + " " + sortOrder ).list();
		
	}

	public List<EatingPlace> searchEatingPlace(String search) {
		return sessionFactory.getCurrentSession()
				.createQuery("from EatingPlace p where lower(p.name) like lower('" + search + "%')" ).list();
	}
	
	
   


}
