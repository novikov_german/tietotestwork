package ee.novikov.german.tieto.service;

import java.util.List;

import ee.novikov.german.tieto.model.Rating;

public interface RatingService {

	public void addRating(Rating rating);
	
	public List getAllRatingWhereEatingPlaceId(int eatingPlaceId);
	
//	public Rating getRatingById(int id);
	
}
