package ee.novikov.german.tieto.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import ee.novikov.german.tieto.model.Rating;
import ee.novikov.german.tieto.service.EatingPlaceService;
import ee.novikov.german.tieto.service.RatingService;

@Controller
@RequestMapping("/")
public class PlaceController {
	@Autowired
	private RatingService ratingService;
	
	@Autowired
	private EatingPlaceService eatingPlaceService; 
	
	@RequestMapping(value="/eatingPlace", method = RequestMethod.GET)
	public String GeneratePage(@RequestParam("eatingPlaceId") int eatingPlaceId, Model model){
		Rating ratingForm = new Rating();
		model.addAttribute("eatingPlace", eatingPlaceService.getEatingPlaceById(eatingPlaceId));
		model.addAttribute("ratingForm", ratingForm);
		model.addAttribute("ratings", ratingService.getAllRatingWhereEatingPlaceId(eatingPlaceId));
		return "eatingPlace";
		
	}
	
	@RequestMapping(value="/eatingPlace", method = RequestMethod.POST)
	public ModelAndView addRating(@Valid Rating rating, BindingResult bindingResult, Model model){
		if (bindingResult.hasErrors()) {
			model.addAttribute("error","All fields must be filled!");
            return new ModelAndView("redirect:" + "/eatingPlace?eatingPlaceId= " +rating.getEatingPlaceId());
        }else{
        	ratingService.addRating(rating);
        	return new ModelAndView("redirect:" + "/eatingPlace?eatingPlaceId= " +rating.getEatingPlaceId());
        }
		
	}
}
