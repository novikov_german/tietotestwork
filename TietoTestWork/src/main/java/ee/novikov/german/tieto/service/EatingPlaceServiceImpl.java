package ee.novikov.german.tieto.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ee.novikov.german.tieto.dao.EatingPlaceDao;
import ee.novikov.german.tieto.model.EatingPlace;

@Service
public class EatingPlaceServiceImpl implements EatingPlaceService {
	
	@Autowired
	private EatingPlaceDao eatingPlace;
	
	@Transactional
	public List<EatingPlace> getAllEatingPlaces() {
		return eatingPlace.getAllEatingPlaces();
	}

	@Transactional
	public EatingPlace getEatingPlaceById(int eatingPlaceId) {
		return eatingPlace.getEatingPlaceById(eatingPlaceId);
	}

	@Transactional
	public List<EatingPlace> getAllEatingPlacesOrder(String sortByColumn, String sortOrder) {
		return  eatingPlace.getAllEatingPlacesOrder(sortByColumn, sortOrder);
	}

	@Transactional
	public List<EatingPlace> searchEatingPlace(String search) {
		return eatingPlace.searchEatingPlace(search);
	}

}
