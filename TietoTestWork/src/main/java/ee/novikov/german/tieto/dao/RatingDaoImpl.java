package ee.novikov.german.tieto.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;


import org.springframework.stereotype.Repository;

import ee.novikov.german.tieto.model.Rating;



@Repository
public class RatingDaoImpl implements RatingDao {
	
    @Autowired
    private SessionFactory sessionFactory;
    
	public void addRating(Rating rating) {
		sessionFactory.getCurrentSession().save(rating);
	}

	public List<Rating> getAllRatingWhereEatingPlaceId(int eatingPlaceId) {
		return sessionFactory.getCurrentSession().createQuery("from Rating where eating_place_id=" + eatingPlaceId ).list();
	}
}
