package ee.novikov.german.tieto.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;



@Entity
@Table(name="Rating")
public class Rating {
	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@NotNull
	@Size(min=1)
	@Column(name = "name")
	private String name;
	
	@Min(1)
	@Column(name = "rating")
	private int rating;
	
	@NotNull
	@Size(min=1)
	@Column(name = "comment")
	private String comment;
	
	@Column(name = "eating_place_id")
	private int eatingPlaceId;
	
	public Rating() {}
	public Rating(int id, String name, int rating, String comment,int eatingPlace) {
		this.id = id;
		this.name = name;
		this.rating = rating;
		this.comment = comment;
		this.eatingPlaceId = eatingPlace;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getRating() {
		return rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}
	
	public int getEatingPlaceId() {
		return eatingPlaceId;
	}
	
	public void setEatingPlaceId(int eatingPlaceId) {
		this.eatingPlaceId = eatingPlaceId;
	}
	
	
	
}
