package ee.novikov.german.tieto.dao;

import java.util.List;

import ee.novikov.german.tieto.model.Rating;

public interface RatingDao {
	
	public void addRating(Rating rating);
	public List getAllRatingWhereEatingPlaceId(int eatingPlaceId);
}
