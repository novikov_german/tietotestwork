package ee.novikov.german.tieto.service;

import java.util.List;

import ee.novikov.german.tieto.model.EatingPlace;

public interface EatingPlaceService {
	public List<EatingPlace> getAllEatingPlaces();
	public EatingPlace getEatingPlaceById(int eatingPlaceId);
	public List<EatingPlace> getAllEatingPlacesOrder(String sortByColumn, String sortOrder);
	public List<EatingPlace> searchEatingPlace(String search);
}
