package ee.novikov.german.tieto.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import ee.novikov.german.tieto.model.EatingPlace;
import ee.novikov.german.tieto.service.EatingPlaceService;
import ee.novikov.german.tieto.service.RatingService;


@Controller
@RequestMapping("/")
public class MainController {
	@Autowired
	private RatingService ratingService;
	
	@Autowired
	private EatingPlaceService eatingPlaceService; 
	
	@RequestMapping(value="/sort", method = RequestMethod.GET)
	public String sortIndexPage(HttpServletRequest request,Model model,@RequestParam("sortByColumn") String sortByColumn,@RequestParam("sortOrder") String sortOrder){
//		model.addAttribute("eatingPlaces", eatingPlaceService.getAllEatingPlacesOrder(sortByColumn,sortOrder));
		
		PagedListHolder<EatingPlace> pagedListHolder = new PagedListHolder<EatingPlace>(eatingPlaceService.getAllEatingPlacesOrder(sortByColumn,sortOrder));
		pagedListHolder.setPageSize(10);
		request.getSession().setAttribute("pagedListHolder", pagedListHolder);
		model.addAttribute("eatingPlaces", pagedListHolder.getPageList());
		return "index";
	}
	
	@RequestMapping(method = RequestMethod.GET)
	public String main(HttpServletRequest request,Model model){
		PagedListHolder<EatingPlace> pagedListHolder = new PagedListHolder<EatingPlace>(eatingPlaceService.getAllEatingPlaces());
		pagedListHolder.setPageSize(10);
		request.getSession().setAttribute("pagedListHolder", pagedListHolder);
		model.addAttribute("eatingPlaces", pagedListHolder.getPageList());
//		model.addAttribute("eatingPlaces", eatingPlaceService.getAllEatingPlaces());
		return "index";
	}
	
	@RequestMapping(value="/navigate",method = RequestMethod.POST)
	public String eatingPlaceNavigateToPage(HttpServletRequest request, Model model,
            @RequestParam String action){
		PagedListHolder<EatingPlace> pagedListHolder =  (PagedListHolder<EatingPlace>) request.getSession().getAttribute("pagedListHolder");
		if (action.equals("next")) {
			pagedListHolder.nextPage();

        } else if (action.equals("previous")) {
        	pagedListHolder.previousPage();
        }
		
		model.addAttribute("eatingPlaces", pagedListHolder.getPageList());

        return "index";
	}
	
	@RequestMapping(value="/search", method = RequestMethod.GET)
	public String search(HttpServletRequest request, Model model,@RequestParam("search") String search){
//		model.addAttribute("eatingPlaces", eatingPlaceService.searchEatingPlace(search));
		PagedListHolder<EatingPlace> pagedListHolder = new PagedListHolder<EatingPlace>(eatingPlaceService.searchEatingPlace(search));
		pagedListHolder.setPageSize(10);
		request.getSession().setAttribute("pagedListHolder", pagedListHolder);
		model.addAttribute("eatingPlaces", pagedListHolder.getPageList());
		return "index";
	}
}
