CREATE TABLE eating_place
(id serial PRIMARY KEY NOT NULL,
 name varchar(20) NOT NULL,
 address varchar(255) NOT NULL,
 avg_rating numeric(3,2) NOT NULL,
 number_of_votes int NOT NULL);
 
 
CREATE TABLE rating
 (id serial PRIMARY KEY NOT NULL,
  name varchar(20) NOT NULL,
  rating int NOT NULL,
  comment text,
  eating_place_id integer REFERENCES eating_place (id));
  

CREATE TRIGGER  trg_rating_changed
AFTER INSERT OR UPDATE OR DELETE ON rating FOR EACH ROW EXECUTE PROCEDURE calculate_rating();


CREATE OR REPLACE FUNCTION public.calculate_rating()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    VOLATILE
    COST 100.0
AS $BODY$BEGIN
	UPDATE eating_place
    SET number_of_votes = (SELECT count(*) FROM rating WHERE eating_place_id = NEW.eating_place_id),
   	avg_rating = (SELECT avg(rating) FROM rating WHERE eating_place_id = NEW.eating_place_id)
    WHERE id = NEW.eating_place_id;
    RETURN NEW;
END;
$BODY$;

INSERT INTO eating_place (name, address, avg_rating, number_of_votes)
VALUES ('Bar Sky', 'Narva mnt 154', 0 , 0),
('Restaurant Maikrahv', 'Raekoja 8', 0 , 0),
('Zanzibar', 'Vilde tee 140', 0 , 0),
('Restoran Neikid', 'Wismari 3', 0 , 0),
('Osmar', 'Narva mnt 100', 0 , 0),
('Restoran Cru', 'Viru 8', 0 , 0),
('Lido', 'Tammsaare tee 146a', 0 , 0),
('Umami Resto', 'Kadaka tee 141', 0 , 0),
('Pirosmani', 'Uliopilaste tee 1', 0 , 0),
('Pirosmani', 'Tartu maantee 157', 0 , 0),
('Kitchen Room', 'P. Pinna 8', 0 , 0),
('Bravo Pizzeria', 'Punane 48', 0 , 0),
('Bliss', 'Mustamae tee 17', 0 , 0),
('Raagupesa', 'Nomme tee 53', 0 , 0),
('Osmar', 'Tammsaare tee 150', 0 , 0),
('Zanzibar', 'Lagna tee 86', 0 , 0),
('Reval Cafe', 'Parnu mnt 45', 0 , 0);


INSERT INTO rating(name, rating, comment, eating_place_id)
	VALUES ('Rozy', 8, 'Nice Place' , 1 ),
	('Rozy', 8, 'Good atmosphere' , 2 ),
	('Gary', 5, 'Nice Place' , 1 ),
	('Harryson', 8, 'Goog place for eat' , 1 ),
	('Marry', 3, 'Food is not tasty' , 1 ),
	('Marry', 8, 'Good Place' , 2 ),
	('Olga', 3, 'Personal is unpleasant' , 3 ),
	('Olga', 9, 'Good Place' , 4 ),
	('Mary', 5, 'So-so' , 8 ),
	('Henry', 10, 'Food is delicious' , 9 ),
	('Marry', 9, 'Goog place for eat' , 5 ),
	('Olga', 2, 'Bad service' , 9 ),
	('German', 8, 'Nice place' , 1 ),
	('German', 5, 'Not bad' , 2 ),
	('David', 10, 'Personal is friendly' , 3 ),
	('David', 9, 'Food is delicious' , 4 ),
	('David', 6, 'Not bad' , 1 ),
	('Rembo', 7, 'Bad service but food is tasty' , 10 );