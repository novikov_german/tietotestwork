<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>  
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="<c:url value="/resources/bootstrap-3.3.7-dist/css/bootstrap.css" />" media="all" />
	<link rel="stylesheet" type="text/css" href="<c:url value="/resources/ratingStars/css/star-rating.css" />" media="all" />
	<script src="<c:url value="/resources/jquery/jquery-3.2.0.js" />"></script>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.js"></script>
	<script src="<c:url value="/resources/bootstrap-3.3.7-dist/js/bootstrap.js" />"></script>
	<script src="<c:url value="/resources/ratingStars/js/star-rating.js" />"></script>
		<meta charset="utf-8">
		<title>German Novikov</title>
</head>
<body>
	<div class="col-md-3"></div>
	<div class="col-md-6">
		<div>
			<h2>${eatingPlace.name }</h2>
			<c:set var="e" value='<%= request.getParameter("error") %>'/>
			<c:if test = '${e != null}' >
				<div class="alert alert-danger" role="alert">${e}</div>
			</c:if>
			<form:form actions="addRating" method="POST" commandName="ratingForm">
				<div class="form-group">
					<form:input path="eatingPlaceId" value="${eatingPlace.id}" class="hidden"/>
				</div>
				<div class="form-group">
					<form:label path="name" for="name">Name</form:label>
					<form:input path="name" />
				</div>
				<div class="form-group">
					<form:label path="comment" for="comment">Comments</form:label>
					<form:input path="comment" />
				</div>
				<div class="form-group">
					<form:label path="rating" for="input-1" class="control-label">Rate place</form:label>
					<form:input path="rating" id="input-1" name="input-1" class="rating rating-loading" data-min="0" data-max="10" data-step="1" />
				</div>
				<a href="/TietoTestWork">Back</a>
				<button type="submit" class="btn btn-default">Submit</button>
			</form:form>
		</div>
		<br>
		<div>
			<c:forEach items="${ratings}" var="rating">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">${rating.name}  (${rating.rating}/10)</h3>
					</div>
					<div class="panel-body">
						${rating.comment}
					</div>
				</div>
			</c:forEach>
		</div>
	</div>
	<div class="col-md-3"></div>
</body>
</html>